<?php

namespace Drupal\calendly\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class ApiConfigurationForm extends ConfigFormBase {

  /**
   * State Manager.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $stateManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $stateManager) {
    parent::__construct($config_factory);
    $this->stateManager = $stateManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'calendly_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'calendly.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('calendly.settings');
    $form['api_key'] = [
      '#type' => 'textarea',
      '#wysiwyg' => FALSE,
      '#required' => TRUE,

      '#title' => $this->t('Api Key'),
      '#description' => $this->t('Please copy the api Key from <a href="https://calendly.com/integrations/api_webhooks target="_blank">Calendly Api</a>.'),
      '#default_value' => $config->get('api_key'),
    ];

    $form['org_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      // '#wysiwyg' => FALSE,
      '#title' => $this->t('Organization Id'),
      '#description' => $this->t('Organization id information.'),
      '#default_value' => $config->get('org_id'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('calendly.settings')
      ->set('api_key', $values['api_key'])
      ->set('org_id', $values['org_id'])
      ->save();
  }

}
