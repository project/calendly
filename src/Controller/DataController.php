<?php

namespace Drupal\calendly\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\block_content\Entity\BlockContent;

/**
 * Class DataController Information.
 *
 * @package Drupal\calendly\Controller
 */
class DataController extends ControllerBase {
  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * Present the events from Calendly.
   */
  public function eventslist() {
    $config = \Drupal::config('calendly.settings');

    $events_info = $config->get('events_info');

    if (!empty($events_info)) {
      $event_list = json_decode($events_info, TRUE);
      $header = [
        'name' => t('Name'),
        'scheduling_url' => t('Schedule URL'),
        'created_at' => t('Created At'),
        'type' => t('Type'),
        'type_desc' => t('Type Description'),
        'event_type' => t('Event Type'),
        'link' => t('Link'),
      ];

      foreach ($event_list['collection'] as $key => $value) {
        $final_data[$key]['name'] = $value['name'];
        $final_data[$key]['scheduling_url'] = $value['scheduling_url'];
        $final_data[$key]['created_at'] = $value['created_at'];
        $final_data[$key]['kind'] = $value['kind'];
        $final_data[$key]['kind_description'] = $value['kind_description'];
        $final_data[$key]['type'] = $value['type'];

        $block_url = Url::fromRoute('calendly.generateblock',
          [
            'id' => base64_encode($value['name']),
            'url' => base64_encode($value['scheduling_url']),
          ])->toString();

        $final_data[$key]['#markup'] = new FormattableMarkup('<a href=":link">Generate Block</a>!', [':link' => $block_url]);
      }

      $build['tables'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $final_data,
        '#empty' => t('No data found'),

      ];
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    else {

      $build = [
        '#markup' => $this->t('Currently No Events are available, please check the Api Key.'),
      ];
    }
    return $build;

  }

  /**
   * Fetch the values from Calendly.
   */
  public function fetchdata() {

    $config = \Drupal::config('calendly.settings');
    $api_key = $config->get('api_key');
    $org_id = $config->get('org_id');

    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => "https://api.calendly.com/event_types?organization=" . $org_id . "&count=20",
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => [
        "Authorization: Bearer $api_key",
        "Content-Type: application/json",
      ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    $config = \Drupal::getContainer()->get('config.factory')
      ->getEditable('calendly.settings');
    $config->set('events_info', $response)->save();

    if ($err) {
      echo "cURL Error #:" . $err;
      \Drupal::messenger()->addError("cURL Error #:" . json_encode($err));

    }
    else {

      \Drupal::messenger()->addMessage('Updated Event list');

    }
    return new RedirectResponse(
      Url::fromRoute('calendly.eventslist')->toString()
    );

  }

  /**
   * Generate block for Calendly Events.
   */
  public function generateblock($id, $url) {

    $block_data = '<!-- Calendly inline widget begin -->
        <div class="calendly-inline-widget" data-url="' . base64_decode($url) . '"></div>
        <script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js" async></script>
        <!-- Calendly inline widget end -->';
    $block = BlockContent::create([
      'info' => base64_decode($id),
      'type' => 'basic',
      'langcode' => 'en',
    ]);
    $block->body->value = $block_data;
    $block->body->format = 'full_html';

    $block->save();
    \Drupal::messenger()->addMessage('Block Created Successfully!');
    return new RedirectResponse(
      Url::fromRoute('calendly.eventslist')->toString());

  }

}
