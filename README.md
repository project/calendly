# Description
 * Introduction
 * Configuration
 * Installation
 * Maintainers
 * Upcoming Versions

## INTRODUCTION

  Drupal Calendly helps you add Calendly scheduling pages to your Drupal in an easy and simple way.

## CONFIGURATION

  Please visit Calendly config page and update the Api Key & Organization info and submit.
  Visit to Calendly Events List , here you can get the list after click on Event sync.
  Click on Generate block to Generate the block content.

## INSTALLATION
  - drush en calendly -y
  - composer require drupal/calendly

## MAINTAINERS

Supporting by :

  - ITT Digital - <https://www.drupal.org/itt-digital>
  - Subbarao Talla (phpsubbarao) - <https://www.drupal.org/u/phpsubbarao>

## UPCOMING VERSIONS
   
  - Design the block widget colors
  - Automatic Generate the blocks.